import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      firstname: Joi.string().required(),
      lastname: Joi.string().required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      firstname: Joi.string().required(),
      lastname: Joi.string().required()
    },
    params: {
      userId: Joi.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  },
  createReward:{
    body: {
      rewardName: Joi.string().required(),
      rewardPoints: Joi.string().required()
    }
  }
};
