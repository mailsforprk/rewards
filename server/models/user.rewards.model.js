import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import autoIncrement from 'mongoose-auto-increment';

/**
 * User Reward Schema
 */
autoIncrement.initialize(mongoose.connection);
const UserRewardsSchema = new mongoose.Schema({
  rewardName: {
    type: String,
    required: true
  },
  rewardPoints: {
    type: Number,
    required: true,
  },
  rewardedBy:{
    type:String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  createdBy: {
    type:String,
  },
  rewardedTo: {
    type:Number,
    ref: 'User'
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserRewardsSchema.method({
});

/**
 * Statics
 */
UserRewardsSchema.statics = {
  /**
   * Get rewards
   * @param {ObjectId} id - The objectId of rewards.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .exec()
      .then((rewards) => {
        if (rewards) {
          return rewards;
        }
        const err = new APIError('No such rewards exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef Reward
 *
 */
UserRewardsSchema.plugin(autoIncrement.plugin, {model: 'Rewards', startAt: 1000});
export default mongoose.model('Rewards', UserRewardsSchema);
