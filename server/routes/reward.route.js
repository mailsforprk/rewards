import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import rewardCtrl from '../controllers/reward.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
/** GET /api/rewards - Get list of rewards */
  .get(rewardCtrl.list)

  /** POST /api/rewards - Create new reward */
  .post(validate(paramValidation.createReward), rewardCtrl.create);

router.route('/:rewardId')
/** GET /api/rewards/:rewardId - Get reward */
  .get(rewardCtrl.get)

  /** PUT /api/rewards/:rewardId - Update reward */
  .put(rewardCtrl.update)

  /** DELETE /api/rewards/:rewardId - Delete reward */
  .delete(rewardCtrl.remove);

/** Load reward when API with rewardId route parameter is hit */
router.param('rewardId', rewardCtrl.load);

export default router;
