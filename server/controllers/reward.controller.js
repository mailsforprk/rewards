import Reward from '../models/user.rewards.model';

/**
 * Load reward and append to req.
 */
function load(req, res, next, id) {
  Reward.get(id)
    .then((reward) => {
      req.reward = reward; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get reward
 * @returns {reward}
 */
function get(req, res) {
  return res.json(req.reward);
}

/**
 * Create new reward
 * @property {string} req.body.rewardname - The rewardname of reward.
 * @property {string} req.body.mobileNumber - The mobileNumber of reward.
 * @returns {Reward}
 */
function create(req, res, next) {
  const reward = new Reward({
    rewardName: req.body.rewardName,
    rewardPoints: req.body.rewardPoints,
  });

  reward.save()
    .then(savedReward => res.json(savedReward))
    .catch(e => next(e));
}

/**
 * Update existing reward
 * @property {string} req.body.rewardname - The rewardname of reward.
 * @property {string} req.body.mobileNumber - The mobileNumber of reward.
 * @returns {Reward}
 */
function update(req, res, next) {
  const reward = req.reward;
  reward.rewardname = req.body.rewardname;
  reward.mobileNumber = req.body.mobileNumber;

  reward.save()
    .then(savedReward => res.json(savedReward))
    .catch(e => next(e));
}

/**
 * Get reward list.
 * @property {number} req.query.skip - Number of rewards to be skipped.
 * @property {number} req.query.limit - Limit number of rewards to be returned.
 * @returns {Reward[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Reward.list({ limit, skip })
    .then(rewards => res.json(rewards))
    .catch(e => next(e));
}

/**
 * Delete reward.
 * @returns {Reward}
 */
function remove(req, res, next) {
  const reward = req.reward;
  reward.remove()
    .then(deletedReward => res.json(deletedReward))
    .catch(e => next(e));
}

export default { load, get, create, update, list, remove };
